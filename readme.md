- Place "**install-uninstall.sh**" inside your repository.

- Under a folder named "**root**", place all the files organised as you wanted the target system to have them installed.

- If you software requires compiling or getting external files for building "root", create a "**build.sh**" with the recipe to do so.

- In "**info/dependencies.txt**", list the external files that your program needs to run. Like this:

```
"curl"  "/usr/bin/curl"
"libssh"  "usr/include/libssh/libssh.h"
"args"  "/usr/bin/args"  "https://gitlab.com/es20490446e/args"
"so"  "/usr/bin/so"  "https://gitlab.com/es20490446e/so"
```

- If you also need to list dependencies only needed for **building**, use the following ubications instead:

```
info/dependencies/installing.txt
info/dependencies/building.txt
```

- Optionally in "**recipes.sh**" define any of the following extra functions:

```
pre_install
post_install

pre_remove
post_remove

pre_upgrade
post_upgrade
```
